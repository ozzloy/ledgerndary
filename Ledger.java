/*
  to run:

  javac -Xlint:deprecation Ledger.java && java Ledger
*/

import java.util.ArrayList;

public class Ledger {
    double money;
    ArrayList<Double> transactions = new ArrayList<Double>();

    public void deposit(double amount){
	if(0 < amount){
	    transactions.add(Double.valueOf(amount));
	    money += amount;}
	else{
	    p("only deposit positive amounts, gosh dangit!");}
    }

    public void withdraw(double amount){
	if((amount < money) && (0 < amount)){
	    transactions.add(Double.valueOf(amount));
	    money -= amount;
	    return;
	}
	else{
	    if(amount <= 0){
		p("only withdraw positive amounts");}
	    if(money < amount){
		p("you can't withdraw more money than you have");}}}

    public void old_withdraw(double amount){
	if(amount < money){
	    if(0 < amount) {
		transactions.add(Double.valueOf(amount));
		money -= amount;}
	    else{
		p("only withdraw positive amounts");}}
	else{
	    p("you can't withdraw more money than you have");}}
	

    public static void p(String s){
	System.out.println(s);}

    public static void main(String[] args){
	Ledger ledger = new Ledger();
	Ledger ledger2 = new Ledger(9999999999999999999999999);
	ledger.money = 0;
	p("at first, ledger.money = " + ledger.money);
	int deposit_amount = 100;
	ledger.deposit(deposit_amount);
	p("after depositing $"
	  + deposit_amount
	  + ", ledger.money = "
	  + ledger.money);
	int withdraw_amount = 40;
	ledger.withdraw(withdraw_amount);
	p("after withdrawing $"
	  + withdraw_amount
	  + ", ledger.money = "
	  + ledger.money);
	for(Double transaction : ledger.transactions){
	    p("" + transaction);}}}
