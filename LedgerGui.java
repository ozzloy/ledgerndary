/* taken from here:
   https://www.guru99.com/java-swing-gui.html

   to run:
   javac -Xlint:deprecation LedgerGui.java && java LedgerGui
*/

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JTextField;

class LedgerGui{
    public static void p(String s){
	System.out.println(s);}
    public static void main(String args[]){
	Ledger ledger = new Ledger();
	ledger.money = 0;
	JFrame frame = new JFrame("Ledger");
	frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

	// money
	JLabel moneyLabel = new JLabel("money = " + ledger.money);

	// deposit row and its contents
	JPanel depositPane = new JPanel();
	depositPane.setLayout(new BoxLayout(depositPane,
					    BoxLayout.LINE_AXIS));
	JTextField depositTextField = new JTextField("deposit");
	JButton depositButton = new JButton("deposit");
	depositButton.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent e){
		    int amount = Integer.parseInt(depositTextField.getText());
		    ledger.deposit(amount);
		    moneyLabel.setText("money = " + ledger.money);}});
	depositPane.add(depositTextField);
	depositPane.add(depositButton);

	// withdraw row and its contents
	JPanel withdrawPane = new JPanel();
	withdrawPane.setLayout(new BoxLayout(withdrawPane,
					     BoxLayout.LINE_AXIS));
	JTextField withdrawTextField = new JTextField("withdraw");
	JButton withdrawButton = new JButton("withdraw");
	withdrawButton.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent e){
		    int amount = Integer.parseInt(withdrawTextField.getText());
		    ledger.withdraw(amount);
		    moneyLabel.setText("money = " + ledger.money);}});
	withdrawPane.add(withdrawTextField);
	withdrawPane.add(withdrawButton);

	// transaction list
	//JList transactionList = new JList(ledger.transactions);

	// whole frame and its contents
	JPanel contentPane = new JPanel();
	contentPane.setLayout(new BoxLayout(contentPane,
					    BoxLayout.PAGE_AXIS));
	contentPane.add(depositPane);
	contentPane.add(withdrawPane);
	contentPane.add(moneyLabel);
	frame.add(contentPane);
	frame.pack();
	frame.setVisible(true);}}
